<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/food', 'HomeController@food');
Route::get('/drink', 'HomeController@drink');
Route::get('/about', 'HomeController@about');
Route::get('/contact', 'HomeController@contact');
Route::get('/food/cari', 'HomeController@cari')->name('cari');
Route::get('/detail/{slug}', 'HomeController@detail')->name('detail');


Route::prefix('admin')
    ->namespace('Admin')->middleware(['auth', 'admin'])
    ->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');

        Route::resource("food", "FoodController");

        Route::resource("drink", "DrinkController");
    });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
