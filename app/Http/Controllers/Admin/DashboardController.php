<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use App\Drink;
use App\Food;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $data['foods'] = Food::all()->count();
        $data['drinks'] = Drink::all()->count();
        alert()->info('Di Halaman Dashboard Admin', 'Selamat Datang');
        return view('pages.admin.dashboard', $data);
    }
}
