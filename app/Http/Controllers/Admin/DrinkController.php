<?php

namespace App\Http\Controllers\Admin;

use App\Drink;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DrinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['drinks'] = Drink::all();
        return view('pages.admin.drink.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.drink.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'drink_name', 'region',  'picture', 'description', 'video' => 'required',
        ]);

        $drink = new Drink();
        $drink->drink_name = $request->drink_name;
        $drink->region = $request->region;
        $drink->video = $request->video;
        $drink->description = $request->description;
        $drink->slug = Str::slug($request->drink_name, '-');

        if ($request->file('picture')) {
            $fileGet = $request->picture;
            $fileName = $fileGet->getClientOriginalName();

            $fileExtension1 = substr($fileName, -4);
            $fileExtension2 = substr($fileName, -5);

            if ($fileExtension1 == ".jpg" || $fileExtension1 == ".png" || $fileExtension2 == ".jpeg") {
                $move = $fileGet->move('uploads/drink', $fileName);
                $drink->picture = $fileName;

                $drink->save();
                alert()->success('Sukses !', 'Minuman Ditambahkan.');
                return redirect()->route('drink.index');
            } else {
                alert()->error("Error !", 'Masukkan File Gambar Yang Sesuai!');
                return redirect()->route('drink.create');
            }
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Drink::findOrFail($id);

        return view(
            'pages.admin.drink.edit',
            [
                'item' => $item
            ],
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slug = Str::slug($request->drink_name, '-');
        if (!empty($request->file('picture'))){

            if ($request->file('picture')) {
                $fileGet = $request->picture;
                $fileName = $fileGet->getClientOriginalName();
    
                $fileExtension1 = substr($fileName, -4);
                $fileExtension2 = substr($fileName, -5);
    
                if ($fileExtension1 == ".jpg" || $fileExtension1 == ".png" || $fileExtension2 == ".jpeg") {
                    $move = $fileGet->move('uploads/drink', $fileName);
                    Drink::where('id', $id)
                        ->update([

                            'picture' => $fileName,
                            'drink_name' => $request->drink_name,
                            'region' => $request->region,
                            'description' => $request->description,
                            'video' => $request->video,
                            'slug' => $slug

                        ]);
                    alert()->success('Sukses !', 'Minuman Diubah.');
                    return redirect()->route('drink.index');
                } else {
                    alert()->error("Error !", 'Masukkan File Gambar Yang Sesuai!');
                    return redirect()->route('drink.edit',$id);
                }
            }
        }
        else{
            Drink::where('id', $id)
            ->update([
                'drink_name' => $request->drink_name,
                'region' => $request->region,
                'description' => $request->description,
                'video' => $request->video,
                'slug' => $slug
            ]);
            alert()->success('Sukses !', 'Minuman Diubah.');
            return redirect()->route('drink.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $product = Drink::findOrFail($request->id);
        $product->delete();
        alert()->success('Sukses !', 'Minuman Berhasil dihapus.');
        return redirect()->route('drink.index');
    }
}
