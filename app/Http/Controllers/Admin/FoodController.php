<?php

namespace App\Http\Controllers\Admin;

use App\Food;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alert;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['foods'] = Food::all();
        return view('pages.admin.food', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.create_food');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'food_name', 'region',  'picture', 'description', 'video' => 'required',
        ]);

        $food = new Food;
        $food->food_name = $request->food_name;
        $food->region = $request->region;
        $food->video = $request->video;
        $food->description = $request->description;
        $food->slug = Str::slug($request->food_name, '-');


        if ($request->file('picture')) {
            $fileGet = $request->picture;
            $fileName = $fileGet->getClientOriginalName();

            $fileExtension1 = substr($fileName, -4);
            $fileExtension2 = substr($fileName, -5);

            if ($fileExtension1 == ".jpg" || $fileExtension1 == ".png" || $fileExtension2 == ".jpeg") {
                $move = $fileGet->move('uploads/food', $fileName);
                $food->picture = $fileName;

                $food->save();
                alert()->success('Sukses !', 'Makanan Ditambahkan.');
                return redirect()->route('food.index');
            } else {
                alert()->error("Error !", 'Masukkan File Gambar Yang Sesuai!');
                return redirect()->route('food.create');
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Food::findOrFail($id);
        
        return view(
            'pages.admin.edit_food',
            [
                'item' => $item
            ],
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $slug = Str::slug($request->food_name, '-');
        if (!empty($request->file('picture'))) {

            if ($request->file('picture')) {
                $fileGet = $request->picture;
                $fileName = $fileGet->getClientOriginalName();

                $fileExtension1 = substr($fileName, -4);
                $fileExtension2 = substr($fileName, -5);

                if ($fileExtension1 == ".jpg" || $fileExtension1 == ".png" || $fileExtension2 == ".jpeg") {
                    $move = $fileGet->move('uploads/food', $fileName);
                    Food::where('id', $id)
                    ->update([

                        'picture' => $fileName,
                        'food_name' => $request->food_name,
                        'region' => $request->region,
                        'description' => $request->description,
                        'video' => $request->video,
                        'slug' => $slug

                    ]);
                    alert()->success('Sukses !', 'Makanan Diubah.');
                    return redirect()->route('food.index');
                } else {
                    alert()->error("Error !", 'Masukkan File Gambar Yang Sesuai!');
                    return redirect()->route('food.edit', $id);
                }
            }
        } else {
            Food::where('id', $id)
            ->update([
                'food_name' => $request->food_name,
                'region' => $request->region,
                'description' => $request->description,
                'video' => $request->video,
                'slug' => $slug
            ]);
            alert()->success('Sukses !', 'Makanan Diubah.');
            return redirect()->route('food.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $product = Food::findOrFail($request->id);
        $product->delete();
        alert()->success('Sukses !', 'Produk Berhasil dihapus.');
        return redirect()->route('food.index');
    }
}
