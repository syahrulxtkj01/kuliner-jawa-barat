<?php

namespace App\Http\Controllers;

use App\Drink;
use App\Food;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Request $request)
    {   

        $count = Food::all()->count();
        if ($count < 4) {
            $data['random'] = Food::all();
        }
        else {
            $data['random'] = Food::all()->random(4);
        }


        $count2 = Drink::all()->count();
        if ($count2 < 4){
            $data['random2'] = Drink::all();
        }
        else {
            $data['random2'] = Drink::all()->random(4);
        }
        
        return view('pages.home', $data);
    }

    public function food()
    {
        $data['foods'] = Food::paginate(8);
        return view('pages.food', $data);
    }

    public function drink()
    {   
        $data['drinks'] = Drink::paginate(8);
        return view('pages.drink', $data);
    }
    
    public function about()
    {
        return view('pages.about');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function detail($slug)
    {
        $food = Food::all()->where('slug', $slug)->first();
        if($food == NULL){
            $drink = Drink::all()->where('slug', $slug)->first();
            $this->data['foods'] = $drink;
            return view('pages.detail', $this->data);
        }
        else {
            $this->data['foods'] = $food;
            return view('pages.detail', $this->data);
        }
        // $drink = Drink::all()->where('slug', $slug)->first();

        // $this->data['foods'] = $food;
        // return view('pages.detail', $this->data);
        
    }

    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->search;

        
        $food = DB::table('food')
            ->where('food_name', 'like', "%" . $cari . "%")
            ->first();

        // return $food;

        if(is_null($food)){
            $food = DB::table('drinks')
            ->where('drink_name', 'like', "%" . $cari . "%")
            ->paginate();
            return view('pages.drink', ['drinks' => $food]);
        }
        else {
            $food = DB::table('food')
            ->where('food_name', 'like', "%" . $cari . "%")
            ->paginate();
            return view('pages.food', ['foods' => $food]);
        }

        
        
    }
}
