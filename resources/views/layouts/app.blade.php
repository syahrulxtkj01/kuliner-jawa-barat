<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by TEMPLATED
http://templated.co
Released for free under the Creative Commons Attribution License

Name       : UpRight 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20130526

-->
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>@yield('title')</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <link href="{{ url('backend/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    
    @include('includes.style')
    @stack('addon-style')

    <!--[if IE 6]>
      <link href="default_ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->
  </head>
  <body>

    @include('includes.navbar')

    @yield('content')
    
    @include('includes.footer')

    <a href="#" class="gotopbtn"><i class="fas fa-arrow-circle-up"></i></a>

    @include('includes.script')
    @stack('addon-script')
  </body>
</html>
