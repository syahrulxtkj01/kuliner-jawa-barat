@extends('layouts.app')

@section('title')
    Detail
@endsection

@section('active')
    @section('active')
    <li>
        <a href="{{ url('/') }}" accesskey="1" title="">Beranda</a>
    </li>
    <li>
        <a href="{{ url('/food') }}" accesskey="2" title="">Makanan</a>
    </li>
    <li>
        <a href="{{ url('/drink') }}" accesskey="2" title="">Minuman</a>
    </li>
    <li>
        <a href="{{ url('/about') }}" accesskey="2" title="">Tentang</a>
    </li>
    <li>
        <a href="{{ url('/contact') }}" accesskey="2" title="">Kontak</a>
    </li>
@endsection
@endsection

@section('content')
    <div id="welcome" class="wrapper-style1">
      <div class="title">
        <h2>
          @if($foods->food_name === NULL)
        {{ $foods->drink_name }}
        @else
        {{ $foods->food_name }}
        @endif
        </h2>
        <span class="byline">{{ $foods->region }}</span>
      </div>

      <div class="embed-responsive embed-responsive-21by9">
        <iframe
          class="embed-responsive-item"
      src="{{ $foods->video }}"
          allowfullscreen
        ></iframe>
      </div>
    </div>

    <div id="portfolio" class="wrapper-style1">
      <div class="description">
        <p>
          {!!$foods->description!!}
        </p>
      </div>
    </div>
@endsection