@extends('layouts.app')

@section('title')
    Foodinfo
@endsection

@section('active')
    <li  class="current_page_item">
        <a href="{{ url('/') }}" accesskey="1" title="">Beranda</a>
    </li>
    <li>
        <a href="{{ url('/food') }}" accesskey="2" title="">Makanan</a>
    </li>
    <li>
        <a href="{{ url('/drink') }}" accesskey="2" title="">Minuman</a>
    </li>
    <li>
        <a href="{{ url('/about') }}" accesskey="2" title="">Tentang</a>
    </li>
    <li>
        <a href="{{ url('/contact') }}" accesskey="2" title="">Kontak</a>
    </li>
@endsection

@section('content')
    <!-- Header -->
    <div id="welcome" class="wrapper-style1">
      <div class="title">
        <h2>Kumpulan Informasi Makanan dan Minuman Khas Jawa Barat</h2>
        <span class="byline"
          >Temukan informasi dari beragam menu khas Jawa Barat</span
        >
      </div>
      <div
        id="carouselExampleIndicators"
        class="carousel slide"
        data-ride="carousel"
      >
        <ol class="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            class="active"
          ></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img
              src="{{ url('frontend/images/header-1.jpg') }}"
              class="d-block w-100"
              alt=""
              style="height: 550px"
            />
          </div>
          <div class="carousel-item">
            <img
              src="{{ url('frontend/images/header-2.jpg') }}"
              class="d-block w-100"
              alt=""
              style="height: 550px"
            />
          </div>
          <div class="carousel-item">
            <img
              src="{{ url('frontend/images/header-3.jpg') }}"
              class="d-block w-100"
              alt=""
              style="height: 550px"
            />
          </div>
        </div>
        <a
          class="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
        >
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a
          class="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
        >
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <!-- Main -->
    <div id="portfolio" class="wrapper-style1">
      <div class="title">
        <h2>Most Popular</h2>
        <span class="byline">Menu yang paling banyak dicari</span>
      </div>
      @php
          $kolom = "column";
          $count = 1;
      @endphp
      
      @foreach ($random as $item)
      <div id="{{ $kolom.$count }}" style="margin-bottom: 50px">
        <p>
        <a href="{{ url('/detail',$item->slug) }}" class="image image-full" >
            <img src="{{ asset('uploads/food/'.$item->picture) }}" alt="" style="height: 180px"/>
          </a>
        </p>
        <h2>{{ $item->food_name }}</h2>
        <a href="{{ url('/detail',$item->slug) }}" class="button">Lihat Detail</a>
      </div>
      @php
      $count ++;
      @endphp   
      @endforeach

      @php
          $kolom = "column";
          $count = 1;
      @endphp
      
      @foreach ($random2 as $item)
      <div id="{{ $kolom.$count }}">
        <p>
        <a href="{{ url('/detail',$item->slug) }}" class="image image-full" >
            <img src="{{ asset('uploads/drink/'.$item->picture) }}" alt="" style="height: 180px"/>
          </a>
        </p>
        <h2>{{ $item->drink_name }}</h2>
        <a href="{{ url('/detail',$item->slug) }}" class="button">Lihat Detail</a>
      </div>
      @php
      $count ++;
      @endphp   
      @endforeach
</div>

      

@endsection