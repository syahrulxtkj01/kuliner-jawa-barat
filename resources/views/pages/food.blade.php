@extends('layouts.app')

@section('title')
    Daftar Makanan
@endsection

@section('active')
    <li>
        <a href="{{ url('/') }}" accesskey="1" title="">Beranda</a>
    </li>
    <li class="current_page_item">
        <a href="{{ url('/food') }}" accesskey="2" title="">Makanan</a>
    </li>
    <li>
        <a href="{{ url('/drink') }}" accesskey="2" title="">Minuman</a>
    </li>
    <li>
        <a href="{{ url('/about') }}" accesskey="2" title="">Tentang</a>
    </li>
    <li>
        <a href="{{ url('/contact') }}" accesskey="2" title="">Kontak</a>
    </li>
@endsection

@section('content')

    <!-- Main -->
    <div id="portfolio" class="wrapper-style1">
      <div class="title">
        <h2>Daftar Makanan</h2>
        <span class="byline">Beberapa makanan khas Jawa Barat </span>
      </div>
      
        <div class="row">
            @foreach ($foods as $item)
                <div class="col-md-3">
                    <p>
                        <a href="{{url('/detail',$item->slug)}}" class="image image-full">
                            <img src="{{ asset('uploads/food/'.$item->picture) }}" alt="" style="height: 180px"/></a>
                    </p>
                    <h2 style="min-height: 100px">{{ $item->food_name }}</h2>
                    <a href="{{ url('/detail',$item->slug) }}" class="button" style="margin-bottom: 30px">Lihat Detail</a>
                </div>
            @endforeach
        </div>
        <br>
        {{ $foods->links() }}
    </div>
    <!-- End Main -->
@endsection