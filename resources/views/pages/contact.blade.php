@extends('layouts.app')

@section('title')
    Kontak
@endsection

@section('active')
    <li>
        <a href="{{ url('/') }}" accesskey="1" title="">Beranda</a>
    </li>
    <li>
        <a href="{{ url('/food') }}" accesskey="2" title="">Makanan</a>
    </li>
    <li>
        <a href="{{ url('/drink') }}" accesskey="2" title="">Minuman</a>
    </li>
    <li>
        <a href="{{ url('/about') }}" accesskey="2" title="">Tentang</a>
    </li>
    <li class="current_page_item">
        <a href="{{ url('/contact') }}" accesskey="2" title="">Kontak</a>
    </li>
@endsection

@section('content')

    <!-- Main -->
    <div id="portfolio" class="wrapper-style1">
      <div class="title">
        <h2>Kontak</h2>
        <span class="byline">Temukan Kami Disini</span>
      </div>

      <div class="col justify-content-center">
          <h3>
              Telephone
          </h3>
          
      </div>
      
      <div class="col">
          <p>
              +62 812 1251 2667
          </p>
      </div>

      <div class="col">
          <h3>
              Instagram
          </h3>
      </div>
      
      <div class="col">
          <p>
              devinksnn
          </p>
      </div>

      <div class="col">
          <h3>
              Email
          </h3>
      </div>
      
      <div class="col">
          <p>
              devinka2020@gmail.com
          </p>
      </div>
        
    </div>
    <!-- End Main -->
@endsection