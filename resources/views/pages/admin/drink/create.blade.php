@extends('layouts.admin')

@section('active')

  <li class="nav-item ">
    <a class="nav-link" href="{{ url('/admin') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

  <li class="nav-item ">
    <a class="nav-link" href="{{ route('food.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Makanan</span></a>
  </li>

  <li class="nav-item active">
    <a class="nav-link" href="{{ route('drink.index') }}">
        <i class="fas fa-glass-cheers"></i>
        <span>Minuman</span></a>
  </li>
    
@endsection

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Minuman</h1>
        <nav aria-label="breadcrumb text-right">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('drink.index') }}">Minuman</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
            </ol>
        </nav>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form enctype="multipart/form-data" class="form-horizontal"
                        action="{{route('drink.store')}}" method="POST">
                        @csrf
                        <div class="row">
                             <div class="form-group col-md-6">
                                <label for="input_text">Nama Minuman</label>
                                <input type="text" class="form-control" id="drink_name" name="drink_name"
                                    placeholder="ex : Es Cendol" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="harga_produk">Daerah Asal</label>
                                <input type="text" class="form-control" id="region" name="region"
                                    placeholder="ex : Bandung" required>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="input_textarea">Deskripsi</label>
                                <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="input_file">Masukkan Foto</label><br>
                                <div class="input-group mb-3 col-md-12">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" accept="image/*" class="custom-file-input" id="picture"
                                            name="picture" required>
                                        <label class="custom-file-label" for="input_picture">Pilih Foto</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="input_file">Link Video</label><br>
                                <input type="text" class="form-control" id="video" name="video"
                                    placeholder="ex : https://youtube.com/xxx" required>
                            </div>
                            <button type="submit" class="btn btn-primary col-12">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection