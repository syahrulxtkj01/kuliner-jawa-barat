<!-- Bootstrap core JavaScript-->
  <script src="{{ url('backend/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('backend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ url('backend/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ url('backend/js/sb-admin-2.min.js') }}"></script>

  <!-- Page level plugins -->
  <script src="{{ url('backend/vendor/chart.js/Chart.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ url('backend/js/demo/chart-area-demo.js') }}"></script>
  <script src="{{ url('backend/js/demo/chart-pie-demo.js') }}"></script>

  <!-- DataTabels scripts -->
  <script src="{{ asset('backend') }}/vendor/datatables/jquery.dataTables.js"></script>
  <script src="{{ asset('backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="{{ asset('backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <script>
      $(document).ready(function() {
      $('#example').DataTable();
  } );
  </script>

  <!-- CKEditor scripts -->
  <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
  <script>
    var konten = document.getElementById("konten");
      CKEDITOR.replace(description,{
      language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
  </script>

  <!-- SweetAlert scripts -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <!-- Delete scripts -->
  <script type="text/javascript">
    $(function() {
        $('.btn-danger').click(function() {
            id = $(this).attr('data-id');
            $('#input-id').val(id);
            
            // alert( $('#input-id').val() )
        });
    });
</script>