<!-- Footer -->
<div id="footer" class="container">
    <div>
    <div class="title">
        <h2>Temukan beragam menu lainnya</h2>

        <div class="col-lg-12 col-md-12">
        <form class="mt-2" action="{{route('cari')}}" method="GET">
            <div class="input-group">
            <input
                class="form-control bg-light border-0 small"
                type="text"
                name="search"
                id="search"
                value="{{ old('cari') }}"
                placeholder="Cari Disini..."
                aria-label="Search"
                aria-describedby="basic-addon"
            />
            <div class="input-group-append">
                <button
                class="btn text-white"
                type="submit"
                style="background: #c4376b"
                >
                Cari
                </button>
            </div>
            </div>
        </form>
        </div>
        <br />
    </div>
    </div>
    <p>
    &copy; Foodinfo. | Design by
    <a href="http://templated.co" rel="nofollow">TEMPLATED</a>.
    </p>
</div>